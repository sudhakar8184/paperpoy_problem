const mongoose = require('mongoose')
let isProduction = process.env.NODE_ENV === 'production';
let dblink  = process.env.DB_URI
mongoose.connect(dblink, { useCreateIndex: true, useNewUrlParser: true ,useUnifiedTopology: true },
(err,db)=>{
    if(!err)
    {
      console.log('Database connected successfully');
    }else{
            console.log('mongoose connection failed')
    }
  });
  mongoose.set('debug', true);
