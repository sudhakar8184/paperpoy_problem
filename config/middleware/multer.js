const multer = require('multer');
var path = require('path')
let storage = multer.diskStorage({
    destination : (req,file,cb)=>{
        cb(null,path.join(__dirname,'/../../uploads/'))
    },
    filename: function (request, file, callback) {
        callback(null, file.originalname)
      }
})

const upload = multer({storage})

module.exports = upload