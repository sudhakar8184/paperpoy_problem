const mongoose = require('mongoose')
const router = require('express').Router();
const upload = require('../../middleware/multer')
const uploadcsv = require('../../../app/controllers/uploadcsv')
const getdata = require('../../../app/controllers/getdata')

router.post('/',upload.single('csvfile') ,uploadcsv.csvtojsonData)
router.get('/state' ,getdata.state)
router.get('/town' ,getdata.town)
router.get('/district' ,getdata.district)
module.exports = router
