const mongoose = require('mongoose')
let Schema = mongoose.Schema
var districtSchema = new Schema({
    _id:{
        type:String,
        trim:true,
        required:true,
        unique:true
    },
    state:{
        type:String,
        trim:true,
        required:true
    },
    state_code:{
        type:String,
        trim:true,
        required:true
    },
    district_code:{
        type:String,
        trim:true,
        required:true
    },
    district:{
        type:String,
        trim:true,
        required:true
    }
})

module.exports = mongoose.model('Districts',districtSchema)