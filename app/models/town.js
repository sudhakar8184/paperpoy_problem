const mongoose = require('mongoose')
let Schema = mongoose.Schema
var townSchema = new Schema({
    _id:{
        type:String,
        trim:true,
        required:true,
        unique:true
    },
    town:{
        type:String,
        trim:true,
        required:true
    }, 
    urban_status:{
        type:String,
        trim:true,
        required:true
    },
    district:{
        type: String,
        trim:true,
        required:true,
        ref: 'Districts'
    }
})

module.exports = mongoose.model('Towns',townSchema)