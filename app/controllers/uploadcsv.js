const Mongoose = require('mongoose')
// const User = Mongoose.model('Users')
const csvtojson = require("csvtojson");
const uploadcsvhelper = require("./../helper/uploadcsv");
const fs = require('fs')
module.exports= {
    csvtojsonData : async(req,res) => {
        try {
          console.log(req.body,req.file)
          csvtojson(req,res)
           csvtojson().fromFile(req.file.path).then(async(json) => 
              {
             fs.unlink(req.file.path,function(err,res){})
                try{
                  if(json.length){
                   json = await uploadcsvhelper.csvtojsonData(json)
                   if(json.success)
                    return res.status(201).json({success:true,data:"successfully data inserted"})
                    else
                    throw new Error(json.err)
                  } else 
                    throw new Error('no data persent')
                }catch(err){
                  return res.json({success: false,data:err.message})
                }
                
              })
        }
       catch(err) {
         console.log("error",err)
       return res.json({success: false,data:err.message})
       }
    },
  }