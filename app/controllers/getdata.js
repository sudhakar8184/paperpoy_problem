const Mongoose = require('mongoose')
const District = Mongoose.model('Districts')
const Town = Mongoose.model('Towns')
module.exports = {
  state: async (req, res) => {
    try {
      if (req.query.q) {
        let stateData = await District.find({ state: { $regex: new RegExp(req.query.q, "gmi") } }, { state: 1, district: 1, district_code: 1 }).select('-_id').lean()
        if (stateData.length)
          return res.json({ success: true, data: stateData })
        else
          return res.json({ success: true, data: 'no data is present' })
      } else
        throw new Error('please search something')

    }

    catch (err) {
      console.log("error", err)
      return res.json({ success: false, data: err.message })
    }
  },

  town: async (req, res) => {
    try {
      if (req.query.q) {
        let townData = await Town.find({ town: { $regex: new RegExp(req.query.q, "gmi") } }).
          populate({
            path: 'district',
            select: 'district state -_id'
          }).select('-_id -urban_status -__v').lean()
        if (townData.length) {
          townData = dataformat(townData)
          return res.json({ success: true, data: townData })
        } else {
          return res.json({ success: true, data: 'no data is present' })
        }

      } else
        throw new Error('please search something')
    }

    catch (err) {
      console.log("error", err)
      return res.json({ success: false, data: err.message })
    }
  },
  district: async (req, res) => {
    try {
      if (req.query.q) {
        let districtData = await Town.find({ 'district': { $regex: new RegExp(req.query.q, "gmi") } }).populate({
          path: 'district',
          select: '-_id -__v'
        }).select('-_id -__v').lean()
        if (districtData.length) {
       districtData = dataformat(districtData,true)
          return res.json({ success: true, data: districtData })
        } else {
          return res.json({ success: true, data: 'no data is present' })
        }

      } else
        throw new Error('please search something')

    }

    catch (err) {
      console.log("error", err)
      return res.json({ success: false, data: err.message })
    }
  },
}

function dataformat(results,falg = false) {
  let newResults = []
  results.map((data) => {
    let newdata = data.district
    delete data['district']
    data = Object.assign(data,newdata)
  })
  return results
}