const Mongoose = require('mongoose')

const Town = Mongoose.model('Towns')
const District = Mongoose.model('Districts')
module.exports= {
    csvtojsonData : async(jsonData) => {
        try {
           jsonData=JSON.stringify(jsonData)
            if(jsonData.indexOf('District')>0&&jsonData.indexOf('City/Town')>0){
               jsonData= jsonData.replace(/State\/ *Union territory\*/gmi,'state')
               jsonData = JSON.parse(jsonData)
               let dis={}
               let town = {}
               jsonData.map((data)=>{
                  let uniqueId = data['City/Town']+data['Urban Status']+data['District Code']
                  if(!town[uniqueId]){
                     let tow = {
                        _id: uniqueId,
                        town:data['City/Town'],
                        urban_status: data['Urban Status'],
                        district:data['District']
                     }
                     town[uniqueId]= tow
                  }
                  if(!dis[data['District']]){
                     let district = {
                        _id: data['District'],
                        state:data['state'],
                        state_code: data['State Code'],
                        district:data['District'],
                        district_code:data['District Code']
                     }
                     dis[data['District']]= district 
                  }
               })
            town=   await Town.insertMany(Object.values(town))
           dis =    await District.insertMany(Object.values(dis))
        return { success:true,town,dis}       
            } else 
            throw new Error('this is not exact file')
           
        }
       catch(err) {
          throw new Error(err)
       }
    }
  }

 