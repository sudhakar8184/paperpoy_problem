require('dotenv').config({ silent: true });
const express = require('express');
const bodyparser = require('body-parser');
const cors = require('cors');
const path =require('path')
var app = express();
app.use(bodyparser());
app.use(cors());
app.use(express.static(path.join(__dirname ,'./views')));
module.exports = app;
require('./../config/routes')(app)
require('./../config/db')